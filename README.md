# Base de données

Dépot du modèle de la BD OCARHY
Version 1.0

## Note sur le partage de la BD

• La base de données est stockées sur un serveur qui gère la base de données.
• Développée sur PostGres/PostGIS (+ extension raster)
• Il s'agit d'un modèle de base. Toutes les tables sont donc vidées.

### Elements clés de la base

#### 5 Schémas :
- **donnees_diag** : Contients toutes les données éditables par les utilisateurs
	• table de diagnostic / table de géometrie / table d'élément commun / table photo / Vues d'édition
- **donnees_refentiel** : Contient toutes les données de visualisation
	• Table de visualisation
- **projet_qgis** : Stocke les projets QGIS du projet OCARHY
	• Projets Qgis QGZ 
- **[A créer]** **dictionnaire** : 
	• Les dictionnaires de valeurs
- **public** : Contient les requis de fonctionnement de la base	
	• layers-styles : contient les formulaires de saisies que l'on va appeler sur QGIS

**/NOTE\** donnees_diag est le premier schéma de la base, quand Charente Eaux a créé le modèle de la base, tout était fait dans ce schéma, les autres et prochains schémas sont arrivés bien plus tardivement dans le processus de création de la table. C'est donc pour cela que certains données de visualisation se retrouvent encore dans ce schéma. (je dois donc faire ce travail de nettoyage)

#### Fonctionnement des données d'édition :
- 1 **table de diagnostic** (Abreuvement, ripisylve, facies...) : Elle contient toutes les informations liée à la table.
- 1 **table de géométrie** (element_lineaire, element_ponctuel, element_zone) : Elle contient l'information de géométrie. Toutes les tables de diagnotic sont reliées aux tables de géométrie en fonction de leur géométrie par une relation x-x (facies est lié a element_lineaires, là où rejets est lié à elements_ponctuels puisqu'il s'agit d'une géométrie de point)
- 1 **table d'éléments communs** [elt_diag] : Elle contient tous les champs communs entres toutes les tables de diagnostic (on évite la redondance pour optimiser le poids de la base)
- 1 **table photo** : Contient les chemins des photos des diagnostics (pour éviter la redondance)
- 1 ou plusieurs **vue** (nom_table_de_diagnostic_type_de_géomtrie (Ex : Abreuvement_ponctuel) : Une vue éditable qui compile les 4 tables au dessus. Une table de diagnostic peut rendre plusieurs vues. Par exemple, ouvrage renvoie à 2 vues différentes (une pour une saisie exhaustive, une autre pour une saisie synthétique des ouvrages)

#### Fonctionnement des droits d'accès :
- La base de données contient des données sensibles (comme des données cadastrales qui contiennent des coordonnées de propriétaires). 
- Tous les utilisateurs n'ont donc pas le droits d'accéder à toutes les données et dans certaines données ils n'ont pas le droit d'accéder à certaines entitées
- Nos utilisateurs : des syndicats GEMAPI. Ils correspondent donc à une entité géographique équivalente à l'aire géographique où ils exercent leur compétence.
- Les droits sont donc réfléchit en fonction des périmètres de compétences des syndicats.

- **La table de base** : Syndicat_GEMAPI (geométrie multipolygonale) : Il s'agit de notre table de référence, qui va permettre d'attribuer les droits d'accès aux entités dans les tables.
- **La table user** : Elle répertorie tous les utilisateurs de la base
- **La table user-syndicat** : Elle fait le lien entre un utilisateur et un syndicat. Quand un utilisateur se connecte à la base les RLS vont s'activer et prendre référence sur al table syndicat pour savoir à quel syndicat appartitent l'utilisateur et appliquer les droits.
- **La table d'association** :A chaque ajout de données ou modification de données, un trigger d'association s'active et va stocké dans une table d'association l'ID de l'entité et le syndicat où il s'intersecte.
- **Les RLS** : Chaque table de diagnostic possède une RLS, les RLS n'empêche pas l'accès à une donnée mais empêche l'accès aux entités. Quand un utilisateur souhaite acceder a une table, c'est cet élément de la base qui va s'activer et dire si oui ou non l'utilisateur à accés à tel entité. Pour ce faire, il regarde sur la table d'association diagnostic-syndicat les entités qu'elle peut afficher en fonction du syndicat.

- **Les rôles** : Chaque tables attributs des roles différents en fonction de l'utilisateur.
	- role rivi-writer : permet la saisie, la modification et la suppression d'entité :  Tous les utilisateurs GEMAPI y ont accès pour les données de diagnostic et d'édition, mais ce role n'est pas attribué sur les tables de visualisation
	- role rivi-reader : permet l'affichage des données, tous les utilisateurs GEMAPI ont accès sur toutes les tables. Sauf pour certaines tables précises, par exemple, un syndicat x qui n'a pas signé la convention d'exploitation du syndciat n'a pas de rivi-reader sur la données cadastre

#### Fonctionnement des triggers :
- 1 trigger d'edition de vues : On peut modifier les vues, cela enclenche un trigger à la validation de la modification qui injecte les modifications dans les 4 tables d'éditions
- 1 trigger d'association : A chaque ajout de données ou modification de données, un trigger d'association s'active et va stocké dans une table d'association l'ID de l'entité et le syndicat où il s'intersecte.

## Note version 1.0

La version 1.0 est la première version de la base que je partage sur GitLab.
La base possède quelques problèmes de rangement que je dois optimiser.

Les objectifs pour sortir la version 1.2 : 

- [ ] Supprimer certaines tables dupliquées
- [ ] Virer les tables de référentiel qui sont stockées dans le schéma "donnees_diag" vers le schéma "donnees_referentiel"
- [ ] Envoyer les dictionnaires dans un schéma dédié à créer (je compte l'appeler : "dictionnaire")

Objectif d'update : Mars 2022 vers la version 1.2.